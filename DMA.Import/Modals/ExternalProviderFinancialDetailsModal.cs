﻿using System;
using System.Collections.Generic;
using System.Text;
using DMA.Import.Interfaces;

namespace DMA.Import.Modals
{
  public class ExternalProviderFinancialDetailsModal : IExternalProviderDetailsModal
  {
    public int ExternalProviderEntityID { get; set; }
    public int ExternalProviderDMAID { get; set; }
    public string ExternalProviderDMAContactID { get; set; }
    public string InstrumentCode { get; set; }
    public string ISIN { get; set; }
    public decimal Quantity { get; set; } 
  }
}
