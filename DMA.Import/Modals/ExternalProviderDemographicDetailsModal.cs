﻿using DMA.Import.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DMA.Import.Modals
{
  public class ExternalProviderDemographicDetailsModal : IExternalProviderDetailsModal
  {
    public int ExternalProviderEntityID { get; set; }
    public int ExternalProviderDMAID { get; set; }
    public string ExternalProviderDMAContactID { get; set; }
    public string ExternalAccountID { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string EmailAddress { get; set; }
    public string MobileNumber { get; set; }
    public string CountryCode { get; set; }

    public string PrimaryAddressLine1 { get; set; }
    public string PrimaryAddressLine2 { get; set; }
    public string PrimaryAddressLine3 { get; set; }
    public string PrimaryAddressLine4 { get; set; }
    public string PrimaryAddressLine5 { get; set; }
    public string ResidentialCode { get; set; }

    public string IDNumber { get; set; }
    public bool IsIndividualInd { get; set; }
  }
}
