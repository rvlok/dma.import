﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DMA.Import.Interfaces
{
  public interface IExternalProviderDetailsModal
  {
    int ExternalProviderEntityID { get; set; }

    int ExternalProviderDMAID { get; set; }

    string ExternalProviderDMAContactID { get; set; }
  }
}
